import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { ScrollView } from '../node_modules/react-native-gesture-handler';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
          <ScrollView>
            <Text> Welcome to React Native Homepage! </Text>
            <Button
              title="Details"
              onPress={() => this.props.navigation.navigate('Details')}
            />
            <Button
              title="About Me"
              onPress={() => this.props.navigation.navigate('AboutMe')}
            />
            <Button
              title="Activity Indicator"
              onPress={() => this.props.navigation.navigate('ActivityIndicatorScreen')}
            />
            <Button
              title="Drawer Layout"
              onPress={() => this.props.navigation.navigate('DrawerLayoutScreen')}
            />
            <Button
              title="Image Screen"
              onPress={() => this.props.navigation.navigate('ImageScreen')}
            />
            <Button
              title="Keyboard Avoiding View"
              onPress={() => this.props.navigation.navigate('KeyboardAvoidingViewScreen')}
            />
            <Button
              title="List View"
              onPress={() => this.props.navigation.navigate('ListViewScreen')}
            />
            <Button
              title="Modal"
              onPress={() => this.props.navigation.navigate('ModalScreen')}
            />
            <Button
              title="Picker"
              onPress={() => this.props.navigation.navigate('PickerScreen')}
            />
            <Button
              title="Progress Bar"
              onPress={() => this.props.navigation.navigate('ProgressBarAndroidScreen')}
            />
            <Button
              title="Refresh"
              onPress={() => this.props.navigation.navigate('RefreshControlScreen')}
            />
            <Button
              title="Scroll View"
              onPress={() => this.props.navigation.navigate('ScrollViewScreen')}
            />
            <Button
              title="Section List"
              onPress={() => this.props.navigation.navigate('SectionListScreen')}
            />
            <Button
              title="Slider"
              onPress={() => this.props.navigation.navigate('SliderScreen')}
            />
            <Button
              title="Status Bar"
              onPress={() => this.props.navigation.navigate('StatusBarScreen')}
            />
            <Button
              title="Switch"
              onPress={() => this.props.navigation.navigate('SwitchScreen')}
            />
            <Button
              title="Text Input"
              onPress={() => this.props.navigation.navigate('TextInputScreen')}
            />
            <Button
              title="Text"
              onPress={() => this.props.navigation.navigate('TextScreen')}
            />
            <Button
              title="Highlight"
              onPress={() => this.props.navigation.navigate('TouchableHighlightScreen')}
            />
            <Button
              title="Feedback"
              onPress={() => this.props.navigation.navigate('TouchableNativeFeedbackScreen')}
            />
            <Button
              title="Opacity"
              onPress={() => this.props.navigation.navigate('TouchableOpacityScreen')}
            />
            <Button
              title="View Pager"
              onPress={() => this.props.navigation.navigate('ViewPagerScreen')}
            />
            <Button
              title="View"
              onPress={() => this.props.navigation.navigate('ViewScreen')}
            />
            <Button
              title="Web View"
              onPress={() => this.props.navigation.navigate('WebViewScreen')}
            />
          </ScrollView>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  

HomeScreen.propTypes = {

};

export default HomeScreen;
