import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class DetailsScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Text> AboutMe Screen: </Text>
            <Text> Hi!, I am Shiela. This is my first react native application. </Text>
            <Button
              title="Home"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default DetailsScreen;
