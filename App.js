import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailsScreen'
import AboutMe from './screens/AboutMe'
import ActivityIndicatorScreen from './screens/ActivityIndicatorScreen'
import DrawerLayoutScreen from './screens/DrawerLayoutScreen'
import ImageScreen from './screens/ImageScreen'
import KeyboardAvoidingViewScreen from './screens/KeyboardAvoidingViewScreen'
import ListViewScreen from './screens/ListViewScreen'
import ModalScreen from './screens/ModalScreen'
import PickerScreen from './screens/PickerScreen'
import ProgressBarAndroidScreen from './screens/ProgressBarAndroidScreen'
import RefreshControlScreen from './screens/RefreshControlScreen'
import ScrollViewScreen from './screens/ScrollViewScreen'
import SectionListScreen from './screens/SectionListScreen'
import SliderScreen from './screens/SliderScreen'
import StatusBarScreen from './screens/StatusBarScreen'
import SwitchScreen from './screens/SwitchScreen'
import TextInputScreen from './screens/TextInputScreen'
import TextScreen from './screens/TextScreen'
import TouchableHighlightScreen from './screens/TouchableHighlightScreen'
import TouchableNativeFeedbackScreen from './screens/TouchableNativeFeedbackScreen'
import TouchableOpacityScreen from './screens/TouchableOpacityScreen'
import ViewPagerScreen from './screens/ViewPagerScreen'
import ViewScreen from './screens/ViewScreen'
import WebViewScreen from './screens/WebViewScreen'

const RootStack = StackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    AboutMe: {
      screen: AboutMe,
    },
    ActivityIndicatorScreen: {
      screen: ActivityIndicatorScreen,
    },
    DrawerLayoutScreen: {
      screen: DrawerLayoutScreen,
    },
    ImageScreen: {
      screen: ImageScreen,
    },
    KeyboardAvoidingViewScreen: {
      screen: KeyboardAvoidingViewScreen,
    },
    ListViewScreen: {
      screen: ListViewScreen,
    },
    ModalScreen: {
      screen: ModalScreen,
    },
    PickerScreen: {
      screen: PickerScreen,
    },
    ProgressBarAndroidScreen: {
      screen: ProgressBarAndroidScreen,
    },
    RefreshControlScreen: {
      screen: RefreshControlScreen,
    },
    ScrollViewScreen: {
      screen: ScrollViewScreen,
    },
    SectionListScreen: {
      screen: SectionListScreen,
    },
    SliderScreen: {
      screen: SliderScreen,
    },
    StatusBarScreen: {
      screen: StatusBarScreen,
    },
    SwitchScreen: {
      screen: SwitchScreen,
    },
    TextInputScreen: {
      screen: TextInputScreen,
    },
    TextScreen: {
      screen: TextScreen,
    },
    TouchableHighlightScreen: {
      screen: TouchableHighlightScreen,
    },
    TouchableNativeFeedbackScreen: {
      screen: TouchableNativeFeedbackScreen,
    },
    TouchableOpacityScreen: {
      screen: TouchableOpacityScreen,
    },
    ViewPagerScreen: {
      screen: ViewPagerScreen,
    },
    ViewScreen: {
      screen: ViewScreen,
    },
    WebViewScreen: {
      screen: WebViewScreen,
    },
  },
  {
    initialRouteName: 'Home',
  }
);


export default class App extends React.Component {
  render() {
    return <RootStack/>;
}}

